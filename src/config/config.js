import {ConfigVar} from '../models/config-var.js';
import {WebApi} from '../services/web-api.js';
import {inject} from 'aurelia-framework';
import { EventAggregator } from 'aurelia-event-aggregator';

@inject(WebApi, EventAggregator)
export class Config {
  key = '';
  value = '';
  configVars = [];
  messageErrorAllowed = false;
  messageSuccessAllowed = false;

  constructor(api, ea){
    this.api = api;
    this.ea =  ea;
  }

  attached(){
    this.showProgressHub();
    this.api
      .getConfigVarList()
      .then(configVars => {
        this.configVars = configVars;
        this.dismissProgressHub();
      })
      .catch(() => {
        this.showMessageError();
        this.dismissProgressHub();
      });
  }

  addElement(){
    this.buildConfigVarWithInsertedValues();
    this.key = '';
    this.value = '';
  }

  buildConfigVarWithInsertedValues(){
    let configVar = new ConfigVar(this.key, this.value);
    this.configVars.unshift(configVar);
  }

  removeElement(index){
    this.configVars.splice(index, 1);
  }

  save(){
    this.showProgressHub();
    this.api
      .saveConfigVars(this.configVars)
      .then(() => {
        this.showMessageSuccess();
        this.dismissProgressHub();
      })
      .catch(() => {
        this.showMessageError();
        this.dismissProgressHub();
      });
  }

  showProgressHub(){
    this.ea.publish('showProgressHub');
  }

  dismissProgressHub(){
    this.ea.publish('dismissProgressHub');
  }

  showMessageError(){
    this.messageErrorAllowed = true;
    setTimeout(() => {
        this.messageErrorAllowed = false;
      }, 6000);
  }

  showMessageSuccess(){
    this.messageSuccessAllowed = true;
    setTimeout(() => {
      this.messageSuccessAllowed = false;
    }, 3000);
  }
}
